package totaliserAnswers;
import totaliserQuestion.Totaliser;

import static org.junit.Assert.*;

import org.junit.Test;


public class TotaliserTest {
//test getsum to see if the sum returned is correct
	Totaliser test = new Totaliser();
	
	@Test
	public void testCorrectSum() {
		test.reset();
		test.getSum();
		test.enterValue(4);
		test.enterValue(4);
		test.getSum();
		assertEquals(test.getSum(), 8);
	}
	
	@Test
	public void testParseExpression() {
		test.reset();		
		test.enterValue(3+4);
		test.getSum();
		assertEquals(test.getSum(), 7);
	}
	
	@Test
	public void testthrowsoutofboundsexception() {
		test.reset();
		String str = "deez nuts";
		try {
			test.enterValue(str);
		} catch(Exception e) {
			System.out.println("invalid type");
		}
	}
	
	@Test
	public void testConstructor() {
		assertThat(test.getSum(), instanceOf(int.class));
	}
	
}

		
	
